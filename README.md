# Cloud Function to ingest logs stored in Cloud Store into Chronicle SIEM

This function is based on some examples available at [Chronicle repository](https://github.com/chronicle/ingestion-scripts). However I've modified the code to make it an event based function so that log files are ingested right after it is uploaded into cloud storage.

## How to run this locally

Build the Docker image:

```commandline
docker build -t chronicle_ingest .
```

Run the image and bind the correct ports:

```commandline
docker run --rm -p 8080:8080 -e PORT=8080 chronicle_ingest
```
