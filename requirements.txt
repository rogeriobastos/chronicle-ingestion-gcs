requests==2.31.0
jwt==1.3.1
google-auth==2.23.0
google-cloud==0.34.0
google-cloud-core==2.3.3
google-cloud-secret-manager==2.16.3
google-cloud-storage==2.10.0
