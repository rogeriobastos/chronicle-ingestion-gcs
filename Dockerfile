# Use the official Python image.
# https://hub.docker.com/_/python
FROM python:3.11-slim@sha256:948a09a8f42c0b2f4308ef563dc3b76de59ebfc5d83ccb7ed7a0fbbb6c5b4713

ENV APP_HOME /app
ENV PYTHONUNBUFFERED TRUE

WORKDIR $APP_HOME

RUN pip install gunicorn cloudevents functions-framework
COPY chronicle_ingestion_gcs requirements.txt .
RUN pip install -r requirements.txt

# Run the web service on container startup.
CMD ["functions-framework", "--target=handler", "--signature-type=cloudevent"]
