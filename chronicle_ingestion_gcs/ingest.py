# Copyright 2023 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Main file to ingest data into Chronicle."""

import json
import os
from typing import Any, Dict, List, Optional, Sequence

from google.auth.transport import requests as Requests
from google.oauth2 import service_account
from google.cloud import secretmanager


AUTHORIZATION_SCOPES = ['https://www.googleapis.com/auth/malachite-ingestion']


def get_value_from_secret_manager(resource_path: str) -> str:
    # Create the Secret Manager client.
    client = secretmanager.SecretManagerServiceClient()

    # Access the secret version.
    response = client.access_secret_version(name=resource_path)
    return response.payload.data.decode('UTF-8')


def initialize_http_session() -> Requests.AuthorizedSession:
    service_account_json = json.loads(
        get_value_from_secret_manager(
            os.environ['CHRONICLE_SERVICE_ACCOUNT']
        )
    )
    credentials = service_account.Credentials.from_service_account_info(
      service_account_json, scopes=AUTHORIZATION_SCOPES
    )
    return Requests.AuthorizedSession(credentials)


def ingest(data: str):
    http_session = initialize_http_session()

    customer_id = os.environ['CHRONICLE_CUSTOMER_ID']
    log_type = os.environ['CHRONICLE_DATA_TYPE']
    namespace = os.getenv('CHRONICLE_NAMESPACE')

    # JSON payload to be sent to Chronicle.
    body = {
        "customerId": customer_id,
        "logType": log_type,
        "entries": [],
    }
    if namespace:
        body["namespace"] = namespace

    # Prepare the chunk data
    data_size = len(data)
    data_list = data.splitlines()
    number_of_lines = len(data_list)
    size_of_line = data_size // number_of_lines
    inc = 800000 // size_of_line  # number of lines to fill 800kb
    start = 0
    while start < number_of_lines:
        end = start + inc
        if end > number_of_lines:
            end = number_of_lines

        # Parse the data in a format expected by Ingestion API of Chronicle.
        # The Ingestion API of Chronicle expects log payload in the format of
        # [{"logText": str(log1)}, {"logText": str(log2)}, ...]
        body["entries"] = list(
            map(lambda i: {"logText": i}, data_list[start:end])
        )

        # Ingest logs into Chronicle
        _send_logs_to_chronicle(http_session, body)

        start = end
        body["entries"].clear()


def _send_logs_to_chronicle(
    http_session: Requests.AuthorizedSession,
    body: Dict[str, List[Any]],
):
    """Sends unstructured log entries to the Chronicle backend for ingestion.

    Args:
        http_session (Requests.AuthorizedSession): Authorized session for HTTP
            requests.
        body (Dict[str, List[Any]]): JSON payload to send to Chronicle
            Ingestion API.

    Raises:
        RuntimeError: Raises if any error occured during log ingestion.
    """

    url = "https://malachiteingestion-pa.googleapis.com/v2/unstructuredlogentries:batchCreate"

    header = {"Content-Type": "application/json"}
    log_count = len(body["entries"])

    # Calling the Chronicle Ingestion API.
    # Reference - https://github.com/chronicle/api-samples-python/blob/master/
    #   ingestion/create_unstructured_log_entries.py
    response = http_session.request("POST", url, json=body, headers=header)

    try:
        response.raise_for_status()
        # If the Ingestion API execution is successful, it will return an empty
        # dictionary.
        if not response.json():
            print(f"{log_count} log(s) pushed successfully to Chronicle.")
    except Exception as err:
        raise RuntimeError(
            f"Error occurred while pushing logs to Chronicle. "
            f"Status code {response.status_code}. Reason: {response.json()}"
        ) from err
