import functions_framework
from cloudevents.http.event import CloudEvent
from google.cloud import storage
from ingest import ingest

@functions_framework.cloud_event
def handler(cloud_event: CloudEvent) -> None:
    bucket_name = cloud_event.data['bucket']
    blob_name = cloud_event.data['name']

    print(f'Download {blob_name} from {bucket_name}')

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob_data = blob.download_as_text(encoding='utf-8')

    ingest(blob_data)
